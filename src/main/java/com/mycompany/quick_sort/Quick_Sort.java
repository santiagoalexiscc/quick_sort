/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.quick_sort;
/**
 *
 * @author Juanda,maiken,santiago
 */
public class Quick_Sort {


    public static void quickSort(int[] arr) {
        if (arr == null || arr.length == 0) {
            return;
        }
        quickSort(arr, 0, arr.length - 1);
    }

    private static void quickSort(int[] arr, int left, int right) {
        if (left >= right) {
            return;
        }
        
        // Seleccionar el pivot (en este caso, el último elemento del arreglo)
        int pivot = arr[right];
        
        // Obtener el índice de partición
        int partitionIndex = partition(arr, left, right, pivot);
        
        // Llamadas recursivas para ordenar los subarreglos
        quickSort(arr, left, partitionIndex - 1);
        quickSort(arr, partitionIndex + 1, right);
    }

    private static int partition(int[] arr, int left, int right, int pivot) {
        int i = left - 1;
        
        // Realizar la partición
        for (int j = left; j < right; j++) {
            if (arr[j] <= pivot) {
                i++;
                swap(arr, i, j);
            }
        }
        
        // Mover el pivot a su posición correcta
        swap(arr, i + 1, right);
        
        return i + 1;
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void main(String[] args) {
        int[] arr = {7, 2, 1, 6, 8, 5, 3, 4};
        
        System.out.println("Arreglo original: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        
        quickSort(arr);
        
        System.out.println("\nArreglo ordenado: ");
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }
}
